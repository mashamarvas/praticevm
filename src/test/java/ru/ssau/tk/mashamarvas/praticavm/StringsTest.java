package ru.ssau.tk.mashamarvas.praticavm;

import org.testng.annotations.Test;

import java.nio.charset.StandardCharsets;

import static org.testng.Assert.*;

public class StringsTest {

    @Test
    public void testGetEachSymbolOfString() {
        String string = "Test method getEach SymbolOfString";
        Strings.getEachSymbolOfString(string);
    }

    @Test
    public void testPrintBytes() {
        String firstString = "Test method printBytes";
        Strings.printBytes(firstString);
        System.out.println("\n");
        String secondString = "тестирование метода";
        Strings.printBytes(secondString);
    }

    @Test
    public void testCheckPalindrome() {
        String firstString = "потоп";
        String secondString = "Аркадий";
        assertTrue(Strings.checkPalindrome(firstString));
        assertFalse(Strings.checkPalindrome(secondString));
    }

    @Test
    public void testGetIndexOfFirstEntry() {
        String firstString = "abcabcabc";
        String secondString = "bc";
        String thirdString = "Сунул Грека руку в реку";
        String forthString = "руку";
        assertEquals(Strings.getIndexOfFirstEntry(firstString, secondString), 1);
        assertEquals(Strings.getIndexOfFirstEntry(thirdString, forthString), 12);
        assertEquals(Strings.getIndexOfFirstEntry(firstString, thirdString), -1);
        assertEquals(Strings.getIndexOfFirstEntry(thirdString, secondString), -1);
    }

    @Test
    public void testGetIndexOfFirstEntryInSecondPartOfFirstString() {
        String firstString = "abcabcabc";
        String secondString = "bc";
        String thirdString = "Сунул Грека руку в реку, рак за руку Греку цап";
        String forthString = "руку";
        assertEquals(Strings.getIndexOfFirstEntryInSecondPartOfFirstString(firstString, secondString), 4);
        assertEquals(Strings.getIndexOfFirstEntryInSecondPartOfFirstString(thirdString, forthString), 32);
        assertEquals(Strings.getIndexOfFirstEntryInSecondPartOfFirstString(firstString, thirdString), -1);
        assertEquals(Strings.getIndexOfFirstEntryInSecondPartOfFirstString(secondString, secondString), -1);
    }

    @Test
    public void testGetIndexOfLastEntryInFirstPartOfFirstString() {
        String firstString = "abcabcabc";
        String secondString = "bc";
        String thirdString = "Сунул Грека руку в реку, рак за руку Греку цап";
        String forthString = "руку";
        assertEquals(Strings.getIndexOfLastEntryInFirstPartOfFirstString(firstString, secondString), 4);
        assertEquals(Strings.getIndexOfLastEntryInFirstPartOfFirstString(thirdString, forthString), 12);
        assertEquals(Strings.getIndexOfLastEntryInFirstPartOfFirstString(firstString, thirdString), -1);
        assertEquals(Strings.getIndexOfLastEntryInFirstPartOfFirstString(thirdString, secondString), -1);
    }

    @Test
    public void testGetNumberOfRowsThatSatisfyArguments() {
        String[] strings = {"gagapapa", "gatata", "gatatapa", "opaopa", "tatata"};
        String prefix = "ga";
        String firstSuffix = "pa";
        String secondSuffix = "ta";
        assertEquals(Strings.getNumberOfRowsThatSatisfyArguments(strings, prefix, firstSuffix), 2);
        assertEquals(Strings.getNumberOfRowsThatSatisfyArguments(strings, prefix, secondSuffix), 1);
    }

    @Test
    public void testGetNumberOfRowsThatSatisfyArgumentsWithoutSpaces() {
        String[] strings = {" gagapapa tatata", " gatata papapa", " gatatapa ", " gaopaopa ", " gatatata "};
        String prefix = "ga";
        String firstSuffix = "pa";
        String secondSuffix = "ta";
        assertEquals(Strings.getNumberOfRowsThatSatisfyArgumentsWithoutSpaces(strings, prefix, firstSuffix), 3);
        assertEquals(Strings.getNumberOfRowsThatSatisfyArgumentsWithoutSpaces(strings, prefix, secondSuffix), 2);
    }

    @Test
    public void testReplaceOccurrencesOf() {
        assertEquals(Strings.replaceOccurrencesOf("opopopopo", "opo", "po"), "popppo");
        assertEquals(Strings.replaceOccurrencesOf("akunamatata", "ta", "ba"), "akunamababa");
        assertEquals(Strings.replaceOccurrencesOf("ура ура ура ура ура", "ра", "фа"), "уфа уфа уфа уфа уфа");
    }

    @Test
    public void testGetSubstring() {
        assertEquals(Strings.getSubstring("Зима, холода, одинокие дома", -10, 7), "Зима, х");
        assertEquals(Strings.getSubstring("Всё для тебя - рассветы и туманы", 4, 4), " ");
        assertEquals(Strings.getSubstring("Jingle bells, jingle bells", 5, 3), " ");
        assertEquals(Strings.getSubstring("Самый лучший день заходил вчера", 3, 7), "ый л");
    }

    @Test
    public void testMergerOfStringsWithGap() {
        String[] firstArray = {"He", "has", "a", "hat", "in", "his", "hand"};
        assertEquals(Strings.mergerOfStringsWithGap(firstArray), "He, has, a, hat, in, his, hand");
    }

    @Test
    public void testPrintStringOfNumbers() {
        assertEquals(Strings.printStringOfNumbers(5), "0 1 2 3 4");
        assertEquals(Strings.printStringOfNumbers(4), "0 1 2 3");
        assertEquals(Strings.printStringOfNumbers(3), "0 1 2");
        assertEquals(Strings.printStringOfNumbers(2), "0 1");
        assertEquals(Strings.printStringOfNumbers(1), "0");
        String string = Strings.printStringOfNumbers(10000);
        System.out.println(string);
    }

    @Test
    public void testReplaceEveryEvenCharacterAndFlipString() {
        assertEquals(Strings.replaceEvenSymbolAndFlipString("interesting"), "01n8t6e4e2n0");
        assertEquals(Strings.replaceEvenSymbolAndFlipString("exam"), "m2x0");
        assertEquals(Strings.replaceEvenSymbolAndFlipString("зачёт"), "4ё2а0");
        assertEquals(Strings.replaceEvenSymbolAndFlipString("Institute"), "8t6t4t2n0");
    }

    @Test
    public void testReplaceCharset() {
        String string = "Morning";
        String result = Strings.replaceCharset(string, StandardCharsets.UTF_16LE, StandardCharsets.ISO_8859_1);
        System.out.println("Входная строка с первой кодировкой: " + "\n" + string + "\n" + "Строка после замены кодировки: " + "\n" + result);
    }

    @Test
    public void testSplitStringBySpaceCharacter() {
        String firstString = "very  cold";
        String[] firstArrayOfStrings = Strings.splitStringBySpaceCharacter(firstString);
        assertEquals(firstArrayOfStrings[0], "Very");
        assertEquals(firstArrayOfStrings[1], "");
        assertEquals(firstArrayOfStrings[2], "Cold");
        String secondString = "When in Rome, do as the Romans";
        String[] secondArrayOfStrings = Strings.splitStringBySpaceCharacter(secondString);
        assertEquals(secondArrayOfStrings[0], "When");
        assertEquals(secondArrayOfStrings[1], "In");
        assertEquals(secondArrayOfStrings[2], "Rome,");
        assertEquals(secondArrayOfStrings[3], "Do");
        assertEquals(secondArrayOfStrings[4], "As");
        assertEquals(secondArrayOfStrings[5], "The");
        assertEquals(secondArrayOfStrings[6], "Romans");
        String fourthString = "6 a.m";
        String[] fourthArrayOfStrings = Strings.splitStringBySpaceCharacter(fourthString);
        assertEquals(fourthArrayOfStrings[0], "6");
        assertEquals(fourthArrayOfStrings[1], "A.m");
        //assertEquals(firstArrayOfStrings[2], "Cold");
    }
}

package ru.ssau.tk.mashamarvas.praticavm;

public final class Singleton {
    private static Singleton instance;
    public double element;


    private Singleton(double element) {
        this.element = element;
    }

    public static Singleton getInstance(double element) {
        if (instance == null) {
            instance = new Singleton(element);
        }
        return instance;
    }

    private double sum(double element, double number) {
        return element + number;
    }

    private double multiplication(double element, double number) {
        return element * number;
    }

    private double getElement() {
        return element;
    }

    private void setElement(double element) {
        this.element = element;
    }

    public static void main(String[] args) {
        Singleton firstElement = new Singleton(13);
        firstElement.setElement(10);
        System.out.println("Element is   " + firstElement.getElement());
        System.out.println("Sum element with 16.3  is   " + firstElement.sum(firstElement.getElement(), 16.3));
        System.out.println("Multiplication element with 2.33  is   " + firstElement.multiplication(firstElement.getElement(), 2.33));
    }
}










package ru.ssau.tk.mashamarvas.praticavm;

public class Modifiers {
    //Перед каждым блоком /* */ проверки модификатора убрать /* и посмотреть,
    // как ведут" себя пары модификаторов объявления класа/метода класса/полей класса =)
    //************************ ПРОВЕРКИ ДЛЯ МОДИФИКАТОРОВ ОБЪЯВЛЕНИЯ КЛАССА************************
    /*
    public public class MyClass1{}
    protected public class MyClass2{}
    private public class MyClass3{}
    abstract public class MyClass4{}
    final public class MyClass5{}
    strictfp public class MyClass6{}
    --------------------------------
     */

    /*
    public protected class MyClass1{}
    protected protected class MyClass2{}
    private protected class MyClass3{}
    abstract protected class MyClass4{}
    final protected class MyClass5{}
    strictfp protected class MyClass6{}
    --------------------------------
     */

    /*
    public private class MyClass1{}
    protected private class MyClass2{}
    private private class MyClass3{}
    abstract private class MyClass4{}
    final private class MyClass5{}
    strictfp private class MyClass6{}
    --------------------------------
     */

     /*
    public final class MyClass1{}
    protected final class MyClass2{}
    private final class MyClass3{}
    abstract final class MyClass4{}
    final final class MyClass5{}
    strictfp final class MyClass6{}
    --------------------------------
     */

     /*
    public abstract class MyClass1{}
    protected abstract class MyClass2{}
    private abstract class MyClass3{}
    abstract abstract class MyClass4{}
    final abstract class MyClass5{}
    strictfp abstract class MyClass6{}
    --------------------------------
     */

     /*
    public strictfp class MyClass1{}
    protected strictfp class MyClass2{}
    private strictfp class MyClass3{}
    abstract strictfp class MyClass4{}
    final strictfp class MyClass5{}
    strictfp strictfp class MyClass6{}
    --------------------------------
     */
    /*  ************************ ТАБЛИЦА ДЛЯ МОДИФИКАТОРОВ ОБЪЯВЛЕНИЯ КЛАССА************************
    _________________________________________________________________________________________________
                        public   protected    private    final    abstract    strictfp
    _________________________________________________________________________________________________
            public        -        -           -          +           +          +
            protected     -        -           -          +           +          +
            private       -        -           -          +           +          +
            final         +        +           +          -           -          +
            abstract      +        +           +          -           -          +
            strictfp      +        +           +          +           +          -
     ________________________________________________________________________________________________
*/


    //************************ ПРОВЕРКИ ДЛЯ МОДИФИКАТОРОВ ПОЛЕЙ КЛАССА************************
    /*
    public protected int point1;
    protected protected int point2;
    private protected int point3;
    static protected int point4;
    final protected int point8=4;
    transient protected int point6;
    volatile protected int point7;
    --------------------------------
     */
    /*
    public public int point1;
    protected public int point2;
    private public int point3;
    static public int point4;
    final public int point8=4;
    transient public int point6;
    volatile public int point7;
    --------------------------------
     */
    /*
    public private int point1;
    protected private int point2;
    private private int point3;
    static private int point4;
    final private int point8=4;
    transient private int point6;
    volatile private int point7;
    --------------------------------
     */
     /*
    public static int point1;
    protected static int point2;
    private static int point3;
    static static int point4;
    final static int point8=4;
    transient static int point6;
    volatile static int point7;
    --------------------------------
     */
    /*
    public final int point1=2;
    protected final int point2=2;
    private final int point3=2;
    static final int point4=2;
    final final int point8=4;
    transient final int point6=2;
    volatile final int point7=2;
    --------------------------------
     */
    /*
    public transient int point1;
    protected transient int point2;
    private transient int point3;
    static transient int point4;
    final transient int point8=2;
    transient transient int point6;
    volatile transient int point7;
    --------------------------------
     */
     /*
    public volatile int point1;
    protected volatile int point2;
    private volatile int point3;
    static volatile int point4;
    final volatile int point8=4;
    transient volatile int point6;
    volatile volatile int point7;
    --------------------------------
     */
 /*  ************************ ТАБЛИЦА ДЛЯ МОДИФИКАТОРОВ ПОЛЕЙ КЛАССА************************
    _________________________________________________________________________________________________
                        public   protected    private   static      final    transient   volatile
    _________________________________________________________________________________________________
            public        -        -           -          +           +          +          +
            protected     -        -           -          +           +          +          +
            private       -        -           -          +           +          +          +
            static        +        +           +          -           +          +          +
            final         +        +           +          +           -          +          -
            transient     +        +           +          +           +          -          +
            volatile      +        +           +          +           -          +          -
     ________________________________________________________________________________________________
*/
}

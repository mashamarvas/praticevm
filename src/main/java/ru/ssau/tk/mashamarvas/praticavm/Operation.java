package ru.ssau.tk.mashamarvas.praticavm;

abstract class Operation {
    abstract double apply(double number);

    double applyTriple(double number) {
        return apply(apply(apply(number)));
    }
}
